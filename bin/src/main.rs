use {
    crank::{
        parse::ParseState,
        vm::{stdlib::STDLIB, VMState},
    },
    std::{fs::File, path::Path, process::ExitCode},
};

fn main() -> ExitCode {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Interpreter should be called with 1 argument (script path).");
        return ExitCode::FAILURE;
    }

    let path = Path::new(&args[1]);

    let mut file = match File::open(path) {
        Err(why) => {
            eprintln!("Couldn't open {:?}: {}", path, why);
            return ExitCode::FAILURE;
        }
        Ok(file) => file,
    };

    let pgm = match ParseState::new(&mut file, STDLIB.to_vec()).parse() {
        Err(why) => {
            eprintln!("Couldn't parse: {:?}", why);
            return ExitCode::FAILURE;
        }
        Ok(pgm) => pgm,
    };

    match VMState::new(&pgm, STDLIB.to_vec()).run() {
        Err(why) => {
            eprintln!("Execution halted: {:?}", why);
            ExitCode::FAILURE
        }
        Ok(_) => ExitCode::SUCCESS,
    }
}
