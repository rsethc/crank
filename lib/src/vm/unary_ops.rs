use super::{Operation, VMError, VMState, VMValue};

type UnaryOpFn = fn(inner: VMValue) -> Result<VMValue, VMError>;

#[derive(Debug)]
pub struct UnaryOp {
    inner: Box<dyn Operation>,
    un_fn: UnaryOpFn,
}

impl UnaryOp {
    pub fn new(inner: Box<dyn Operation>, un_fn: UnaryOpFn) -> Self {
        UnaryOp { inner, un_fn }
    }
}

impl Operation for UnaryOp {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        (self.un_fn)(self.inner.perform(vm)?)
    }
}

pub fn unary_negate(inner: VMValue) -> Result<VMValue, VMError> {
    match inner {
        VMValue::Int(inner) => Ok(VMValue::Int(-inner)),
        _ => Err(VMError::InvalidOperation),
    }
}

pub fn unary_logic_not(inner: VMValue) -> Result<VMValue, VMError> {
    match inner {
        VMValue::Int(inner) => Ok(VMValue::Int(match inner {
            0 => 1,
            _ => 0,
        })),
        _ => Err(VMError::InvalidOperation),
    }
}
