use {
    crate::parse::Program,
    script_fn::ScriptCallable,
    std::{cell::RefCell, rc::Rc},
};

pub mod assign;
pub mod binary_ops;
pub mod block;
pub mod consts;
pub mod fncall;
pub mod forloop;
pub mod script_fn;
pub mod stdlib;
pub mod unary_ops;
pub mod var_access;

#[derive(Debug, Clone)]
pub enum VMValue {
    Int(i64),
    Null,
    NativeFn(fn(Vec<VMValue>) -> Result<VMValue, VMError>),
    ScriptFn(Rc<ScriptCallable>),
}

#[derive(Debug)]
pub enum VMError {
    InvalidOperation,
}

pub trait Operation: std::fmt::Debug {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError>;
    fn set(&self, _vm: &mut VMState, _value: VMValue) -> Result<(), VMError> {
        Err(VMError::InvalidOperation)
    }
}

#[derive(Debug)]
pub struct VarFrame {
    vars: Vec<VMValue>,
}

pub struct VMState<'a> {
    pgm: &'a Program,
    frames: Vec<Rc<RefCell<VarFrame>>>,
}

impl<'a> VMState<'a> {
    fn push_frame(&mut self, size: usize) {
        self.frames.push(Rc::new(RefCell::new(VarFrame {
            vars: vec![VMValue::Null; size],
        })))
    }
    fn pop_frame(&mut self) {
        self.frames.pop();
    }
    pub fn new(pgm: &'a Program, provided_globals: Vec<(&str, VMValue)>) -> Self {
        let mut global_frame = VarFrame {
            vars: Vec::with_capacity(provided_globals.len()),
        };
        for gbl in provided_globals {
            global_frame.vars.push(gbl.1);
        }
        Self {
            pgm,
            frames: vec![Rc::new(RefCell::new(global_frame))],
        }
    }
    pub fn run(&mut self) -> Result<(), VMError> {
        self.pgm.pgm.perform(self)?;
        Ok(())
    }
}
