use super::{VMError, VMValue};

fn stdlib_print(args: Vec<VMValue>) -> Result<VMValue, VMError> {
    let mut first = true;
    for arg in args {
        if first {
            first = false;
        } else {
            print!(", ");
        }
        print!("{:?}", arg);
    }
    println!();
    Ok(VMValue::Null)
}
pub const STDLIB: &[(&str, VMValue)] = &[("print", VMValue::NativeFn(stdlib_print))];
