use super::{Operation, VMError, VMState, VMValue};

#[derive(Debug)]
pub struct Assign {
    write_to: Box<dyn Operation>,
    read_from: Box<dyn Operation>,
}

impl Assign {
    pub fn new(write_to: Box<dyn Operation>, read_from: Box<dyn Operation>) -> Self {
        Assign {
            write_to,
            read_from,
        }
    }
}

impl Operation for Assign {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        let value = self.read_from.perform(vm)?;
        self.write_to.set(vm, value.clone())?;
        Ok(value)
    }
}
