use super::{Operation, VMError, VMState, VMValue};

#[derive(Debug, Default)]
pub struct Block {
    operations: Vec<Box<dyn Operation>>,
    pub frame_size: usize,
}

impl Block {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn append(&mut self, op: Box<dyn Operation>) {
        self.operations.push(op);
    }
}

impl Operation for Block {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        vm.push_frame(self.frame_size);
        for operation in &self.operations {
            operation.perform(vm)?;
        }
        vm.pop_frame();
        Ok(VMValue::Null)
    }
}
