use super::{Operation, VMError, VMState, VMValue};

#[derive(Debug)]
pub struct Const {
    value: VMValue,
}

impl Const {
    pub fn new(value: VMValue) -> Self {
        Const { value }
    }
}

impl Operation for Const {
    fn perform(&self, _vm: &mut VMState) -> Result<VMValue, VMError> {
        Ok(self.value.clone())
    }
}

pub fn box_const(value: VMValue) -> Box<Const> {
    Box::new(Const::new(value))
}
