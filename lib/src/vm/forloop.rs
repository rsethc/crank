use super::{Operation, VMError, VMState, VMValue};

#[derive(Debug)]
pub struct ForLoop {
    initial: Box<dyn Operation>,
    condition: Box<dyn Operation>,
    after: Box<dyn Operation>,
    body: Box<dyn Operation>,
    pub frame_size: usize,
}

impl ForLoop {
    pub fn new(
        initial: Box<dyn Operation>,
        condition: Box<dyn Operation>,
        after: Box<dyn Operation>,
        body: Box<dyn Operation>,
        frame_size: usize,
    ) -> Self {
        ForLoop {
            initial,
            condition,
            after,
            body,
            frame_size,
        }
    }
}

fn is_truthy(value: VMValue) -> bool {
    match value {
        VMValue::Int(n) => n != 0,
        VMValue::Null => false,
        VMValue::NativeFn(_) => true,
        VMValue::ScriptFn(_) => true,
    }
}

impl Operation for ForLoop {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        vm.push_frame(self.frame_size);
        self.initial.perform(vm)?;
        while is_truthy(self.condition.perform(vm)?) {
            self.body.perform(vm)?;
            self.after.perform(vm)?;
        }
        vm.pop_frame();
        Ok(VMValue::Null)
    }
}
