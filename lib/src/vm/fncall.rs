use super::{Operation, VMError, VMState, VMValue};

#[derive(Debug)]
pub struct FuncCall {
    func_expr: Box<dyn Operation>,
    arg_exprs: Vec<Box<dyn Operation>>,
}

impl FuncCall {
    pub fn new(func_expr: Box<dyn Operation>) -> Self {
        FuncCall {
            func_expr,
            arg_exprs: Vec::new(),
        }
    }
    pub fn append_arg(&mut self, arg: Box<dyn Operation>) {
        self.arg_exprs.push(arg)
    }
}

impl Operation for FuncCall {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        match self.func_expr.perform(vm)? {
            VMValue::NativeFn(func) => {
                let mut args = Vec::with_capacity(self.arg_exprs.len());
                for i in 0..self.arg_exprs.len() {
                    args.push(self.arg_exprs[i].perform(vm)?);
                }
                func(args)
            }
            VMValue::ScriptFn(func) => {
                let mut args = Vec::with_capacity(self.arg_exprs.len());
                for i in 0..self.arg_exprs.len() {
                    args.push(self.arg_exprs[i].perform(vm)?);
                }
                // args currently unused. to do: use args

                // to do: less wasteful way of pushing/popping the callstack.
                // one stack frame is essentially the vec of all varframes that are
                // required for the current thing to access any vars it needs to refer to
                let prior_stack = vm.frames.clone();
                vm.frames.clone_from(&func.capture_frames);

                func.inner_code.perform(vm)?;

                vm.frames = prior_stack;

                // to do: "return" statements should basically throw an error that contains
                // the return value, and this should be turned into an Ok here.
                Ok(VMValue::Null)
            }
            _ => Err(VMError::InvalidOperation),
        }
    }
}
