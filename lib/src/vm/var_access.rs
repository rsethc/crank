use {
    super::{Operation, VMError, VMState, VMValue},
    std::fmt,
};

#[derive(Clone, Copy)]
pub struct VarLoc {
    pub frame_id: usize,
    pub in_frame: usize,
}

impl fmt::Debug for VarLoc {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}.{}", self.frame_id, self.in_frame)
    }
}

#[derive(Debug)]
pub struct VarAccess {
    location: VarLoc,
}

impl VarAccess {
    pub fn new(location: VarLoc) -> Self {
        VarAccess { location }
    }
}

impl Operation for VarAccess {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        Ok(vm.frames[self.location.frame_id].borrow().vars[self.location.in_frame].clone())
    }
    fn set(&self, vm: &mut VMState, value: VMValue) -> Result<(), VMError> {
        vm.frames[self.location.frame_id].borrow_mut().vars[self.location.in_frame] = value;
        Ok(())
    }
}
