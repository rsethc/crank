use super::{Operation, VMError, VMState, VMValue};

type BinaryOpFn = fn(left: VMValue, right: VMValue) -> Result<VMValue, VMError>;

#[derive(Debug)]
pub struct BinaryOp {
    left: Box<dyn Operation>,
    right: Box<dyn Operation>,
    bin_fn: BinaryOpFn,
}

impl BinaryOp {
    pub fn new(left: Box<dyn Operation>, right: Box<dyn Operation>, bin_fn: BinaryOpFn) -> Self {
        BinaryOp {
            left,
            right,
            bin_fn,
        }
    }
}

impl Operation for BinaryOp {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        (self.bin_fn)(self.left.perform(vm)?, self.right.perform(vm)?)
    }
}

pub fn binary_add(left: VMValue, right: VMValue) -> Result<VMValue, VMError> {
    match left {
        VMValue::Int(left) => match right {
            VMValue::Int(right) => Ok(VMValue::Int(left + right)),
            _ => Err(VMError::InvalidOperation),
        },
        _ => Err(VMError::InvalidOperation),
    }
}

pub fn binary_sub(left: VMValue, right: VMValue) -> Result<VMValue, VMError> {
    match left {
        VMValue::Int(left) => match right {
            VMValue::Int(right) => Ok(VMValue::Int(left - right)),
            _ => Err(VMError::InvalidOperation),
        },
        _ => Err(VMError::InvalidOperation),
    }
}

pub fn binary_mul(left: VMValue, right: VMValue) -> Result<VMValue, VMError> {
    match left {
        VMValue::Int(left) => match right {
            VMValue::Int(right) => Ok(VMValue::Int(left * right)),
            _ => Err(VMError::InvalidOperation),
        },
        _ => Err(VMError::InvalidOperation),
    }
}

pub fn binary_div(left: VMValue, right: VMValue) -> Result<VMValue, VMError> {
    match left {
        VMValue::Int(left) => match right {
            VMValue::Int(right) => Ok(VMValue::Int(left / right)),
            _ => Err(VMError::InvalidOperation),
        },
        _ => Err(VMError::InvalidOperation),
    }
}
