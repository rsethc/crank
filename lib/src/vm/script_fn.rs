use {
    super::{Operation, VMError, VMState, VMValue, VarFrame},
    std::{cell::RefCell, fmt, rc::Rc},
};

pub struct ScriptCallable {
    pub inner_code: Rc<Box<dyn Operation>>,
    pub capture_frames: Vec<Rc<RefCell<VarFrame>>>,
}

impl fmt::Debug for ScriptCallable {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "ScriptCallable {{{:?}}}", self.inner_code)
    }
}
impl ScriptCallable {
    pub fn new(
        inner_code: Rc<Box<dyn Operation>>,
        capture_frames: Vec<Rc<RefCell<VarFrame>>>,
    ) -> Self {
        Self {
            inner_code,
            capture_frames,
        }
    }
}

#[derive(Debug)]
pub struct ScriptFnProducer {
    inner_code: Rc<Box<dyn Operation>>,
}

impl ScriptFnProducer {
    pub fn new(inner_code: Box<dyn Operation>) -> Self {
        Self {
            inner_code: Rc::new(inner_code),
        }
    }
}

impl Operation for ScriptFnProducer {
    fn perform(&self, vm: &mut VMState) -> Result<VMValue, VMError> {
        let mut capture_frames = Vec::with_capacity(vm.frames.len());
        for frame in &vm.frames {
            capture_frames.push(Rc::clone(frame));
        }
        Ok(VMValue::ScriptFn(Rc::new(ScriptCallable::new(
            Rc::clone(&self.inner_code),
            capture_frames,
        ))))
    }
}
