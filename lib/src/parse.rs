use {
    crate::{
        lex::{LexError, LexState, Token},
        vm::{
            assign::*, binary_ops::*, block::*, consts::*, fncall::*, forloop::*, script_fn::*,
            unary_ops::*, var_access::*, *,
        },
    },
    std::{collections::HashMap, io::Read},
};

const SEMICOLONS_REQUIRED: bool = true;
const DECLARATIONS_REQUIRED: bool = true;
const MULTIDECLARE_PROHIBITED: bool = true;

struct Scope {
    pub declarations: HashMap<String, usize>,
}

pub struct ParseState<'a> {
    src: LexState<'a>,
    next_tok: Option<Result<Token, LexError>>,
    scopes: Vec<Scope>,
}

#[derive(Debug, Clone, Copy)]
pub enum ParseError {
    LexError(LexError),
    InvalidLeaf,
    UnclosedParen,
    MissingSemicolon,
    MultiDeclare,
    Undeclared,
    ExpectedIdentifierAfterLet,
    UnexpectedToken,
}

impl From<LexError> for ParseError {
    fn from(value: LexError) -> Self {
        ParseError::LexError(value)
    }
}

pub struct Program {
    pub pgm: Box<dyn Operation>,
}

impl<'a> ParseState<'a> {
    pub fn new(src: &'a mut (dyn Read), provided_globals: Vec<(&str, VMValue)>) -> Self {
        let mut state = Self {
            next_tok: None,
            src: LexState::new(src),
            scopes: Vec::new(),
        };
        state.push_scope();
        for (name, _value) in provided_globals {
            state.specify_new_var(name);
        }
        state
    }
    fn advance(&mut self) {
        self.next_tok = None;
    }
    fn peek(&mut self) -> Result<Token, LexError> {
        if let Some(already) = &self.next_tok {
            already.clone()
        } else {
            let further = self.src.read_token();
            self.next_tok = Some(further.clone());
            further
        }
    }
    fn expect(&mut self, expected: Token) -> Result<(), ParseError> {
        if self.peek()? == expected {
            self.advance();
            Ok(())
        } else {
            Err(ParseError::UnexpectedToken)
        }
    }

    fn push_scope(&mut self) {
        self.scopes.push(Scope {
            declarations: HashMap::new(),
        });
    }
    fn pop_scope(&mut self) -> usize {
        self.scopes.pop().unwrap().declarations.len()
    }
    fn search_existing_var(&mut self, name: &str) -> Option<VarLoc> {
        for frame_id in (0..self.scopes.len()).rev() {
            if let Some(in_frame) = self.scopes[frame_id].declarations.get(name) {
                return Some(VarLoc {
                    frame_id,
                    in_frame: *in_frame,
                });
            }
        }
        None
    }
    fn specify_new_var(&mut self, name: &str) -> VarLoc {
        let frame_id = self.scopes.len() - 1;
        let local_frame = &mut self.scopes[frame_id];
        let in_frame = local_frame.declarations.len();
        local_frame.declarations.insert(name.to_string(), in_frame);
        VarLoc { frame_id, in_frame }
    }
    fn declare_var(&mut self, name: &str) -> Result<VarLoc, ParseError> {
        let frame_id = self.scopes.len() - 1;
        if let Some(in_frame) = self.scopes[frame_id].declarations.get(name) {
            if MULTIDECLARE_PROHIBITED {
                Err(ParseError::MultiDeclare)
            } else {
                Ok(VarLoc {
                    frame_id,
                    in_frame: *in_frame,
                })
            }
        } else {
            Ok(self.specify_new_var(name))
        }
    }
    fn lookup_var(&mut self, name: &str) -> Result<VarLoc, ParseError> {
        if let Some(found) = self.search_existing_var(name) {
            Ok(found)
        } else if DECLARATIONS_REQUIRED {
            Err(ParseError::Undeclared)
        } else {
            Ok(self.specify_new_var(name))
        }
    }

    fn read_leaf(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        match self.peek()? {
            Token::OpenParen => {
                self.advance();
                match self.peek()? {
                    Token::CloseParen => {
                        self.advance();
                        // These parentheses indicate a null.
                        Ok(box_const(VMValue::Null))
                    }
                    _ => {
                        // These parentheses are for an inner expression.
                        let inner = self.read_expr()?;
                        if matches!(self.peek()?, Token::CloseParen) {
                            self.advance();
                            Ok(inner)
                        } else {
                            Err(ParseError::UnclosedParen)
                        }
                    }
                }
            }
            Token::Int(value) => {
                self.advance();
                Ok(box_const(VMValue::Int(value)))
            }
            Token::Identifier(name) => {
                self.advance();
                Ok(Box::new(VarAccess::new(self.lookup_var(&name)?)))
            }
            Token::Func => {
                self.advance();
                let declare_into = if let Token::Identifier(name) = self.peek()? {
                    self.advance();
                    Some(name)
                    // Named function
                } else {
                    None
                    // Anonymous function
                };

                self.expect(Token::OpenBrace)?;
                let inner_code = self.read_block(Token::CloseBrace)?;

                let func_op = Box::new(ScriptFnProducer::new(inner_code));
                Ok(if let Some(declare_into) = declare_into {
                    Box::new(Assign::new(
                        Box::new(VarAccess::new(self.specify_new_var(&declare_into))),
                        func_op,
                    ))
                } else {
                    func_op
                })
            }
            _ => Err(ParseError::InvalidLeaf),
        }
    }

    fn read_unary_post(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        let mut left = self.read_leaf()?;
        loop {
            match self.peek()? {
                Token::OpenParen => {
                    self.advance();
                    // These parentheses are for a function call.
                    let mut call = FuncCall::new(left);
                    if matches!(self.peek()?, Token::CloseParen) {
                        self.advance();
                    } else {
                        loop {
                            call.append_arg(self.read_expr()?);
                            match self.peek()? {
                                Token::Comma => {
                                    self.advance();
                                }
                                Token::CloseParen => {
                                    self.advance();
                                    break;
                                }
                                _ => return Err(ParseError::UnclosedParen),
                            }
                        }
                    }
                    left = Box::new(call);
                }
                _ => return Ok(left),
            }
        }
    }

    fn read_unary_pre(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        match self.peek()? {
            Token::Sub => {
                self.advance();
                Ok(Box::new(UnaryOp::new(self.read_unary_pre()?, unary_negate)))
            }
            Token::LogicNot => {
                self.advance();
                Ok(Box::new(UnaryOp::new(
                    self.read_unary_pre()?,
                    unary_logic_not,
                )))
            }
            _ => self.read_unary_post(),
        }
    }

    fn read_add_sub(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        let mut left = self.read_mul_div()?;
        loop {
            match self.peek()? {
                Token::Add => {
                    self.advance();
                    let right = self.read_mul_div()?;
                    left = Box::new(BinaryOp::new(left, right, binary_add));
                }
                Token::Sub => {
                    self.advance();
                    let right = self.read_mul_div()?;
                    left = Box::new(BinaryOp::new(left, right, binary_sub));
                }
                _ => return Ok(left),
            }
        }
    }

    fn read_mul_div(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        let mut left = self.read_unary_pre()?;
        loop {
            match self.peek()? {
                Token::Mul => {
                    self.advance();
                    let right = self.read_unary_pre()?;
                    left = Box::new(BinaryOp::new(left, right, binary_mul));
                }
                Token::Div => {
                    self.advance();
                    let right = self.read_unary_pre()?;
                    left = Box::new(BinaryOp::new(left, right, binary_div));
                }
                _ => return Ok(left),
            }
        }
    }

    fn read_assign(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        let left = self.read_add_sub()?;
        match self.peek()? {
            Token::Assign => {
                self.advance();
                Ok(Box::new(Assign::new(left, self.read_assign()?)))
            }
            _ => Ok(left),
        }
    }

    fn read_expr(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        self.read_assign()
    }

    fn skip_semicolons(&mut self) -> Result<(), ParseError> {
        while matches!(self.peek()?, Token::Semicolon) {
            self.advance();
        }
        Ok(())
    }

    fn expect_semicolon(&mut self) -> Result<(), ParseError> {
        if SEMICOLONS_REQUIRED && !matches!(self.peek()?, Token::Semicolon) {
            return Err(ParseError::MissingSemicolon);
        };
        Ok(())
    }

    fn read_expr_or_let(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        match self.peek()? {
            Token::Let => {
                self.advance();
                if let Token::Identifier(name) = self.peek()? {
                    self.advance();

                    // Assignment has to be handled specially here, since during parsing the
                    // right side, the variable should not be declared yet, but if this isn't
                    // handled specially then parsing the left side will result in an error
                    // due to the variable not being declared yet.
                    let op: Box<dyn Operation> = if matches!(self.peek()?, Token::Assign) {
                        // "let x = y;" variant
                        self.advance();
                        let right = self.read_expr()?;
                        let decl_loc = self.declare_var(&name)?;
                        let left = Box::new(VarAccess::new(decl_loc));
                        Box::new(Assign::new(left, right))
                    } else {
                        // "let x;" variant
                        self.declare_var(&name)?;
                        box_const(VMValue::Null)
                    };

                    self.expect_semicolon()?;

                    Ok(op)
                } else {
                    Err(ParseError::ExpectedIdentifierAfterLet)
                }
            }
            _ => self.read_expr(),
        }
    }

    fn read_statement(&mut self) -> Result<Box<dyn Operation>, ParseError> {
        match self.peek()? {
            Token::OpenBrace => {
                self.advance();
                self.read_block(Token::CloseBrace)
            }
            Token::For => {
                self.advance();
                self.expect(Token::OpenParen)?;
                self.push_scope();
                let initial = self.read_expr_or_let()?;
                self.expect(Token::Semicolon)?;
                let condition = self.read_expr_or_let()?;
                self.expect(Token::Semicolon)?;
                let after = self.read_expr()?;
                self.expect(Token::CloseParen)?;
                let body = self.read_statement()?;
                Ok(Box::new(ForLoop::new(
                    initial,
                    condition,
                    after,
                    body,
                    self.pop_scope(),
                )))
            }
            _ => {
                let expr = self.read_expr_or_let()?;
                self.expect_semicolon()?;
                Ok(expr)
            }
        }
    }

    fn read_block(&mut self, end_tok: Token) -> Result<Box<dyn Operation>, ParseError> {
        self.push_scope();
        let mut block = Block::new();
        loop {
            self.skip_semicolons()?;
            if self.peek()? == end_tok {
                self.advance();
                block.frame_size = self.pop_scope();
                return Ok(Box::new(block));
            } else {
                block.append(self.read_statement()?);
            }
        }
    }

    pub fn parse(&mut self) -> Result<Program, ParseError> {
        Ok(Program {
            pgm: self.read_block(Token::EOF)?,
        })
    }
}
