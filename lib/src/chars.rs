use {crate::bytes::ByteReadable, std::io::Read};

pub struct CharReadable<'a> {
    src: ByteReadable<'a>,
}

#[derive(Debug, Clone, Copy)]
pub enum DecodeError {
    IOError(std::io::ErrorKind),
    CleanEOF, // Signals an EOF that was encountered before partially decoding a new character.
    UnexpectedContinuationByte,
    InvalidContinuationByte,
    InvalidStartByteFlags,
    InvalidCharCast,
}

impl From<std::io::Error> for DecodeError {
    fn from(value: std::io::Error) -> Self {
        DecodeError::IOError(value.kind())
    }
}

impl<'a> CharReadable<'a> {
    pub fn new(src: &'a mut dyn Read) -> Self {
        Self {
            src: ByteReadable::new(src),
        }
    }
    fn continuation_byte(&mut self) -> Result<u32, DecodeError> {
        let byte = self.src.read_byte()?;
        if byte & (0x80 | 0x40) != 0x80 {
            Err(DecodeError::InvalidContinuationByte)
        } else {
            Ok(byte as u32 & 0x3F)
        }
    }
    pub fn read_char(&mut self) -> Result<char, DecodeError> {
        // TO DO: Decode UTF8 from potentially several returns from ReadByte.
        //Ok(self.src.read_byte()? as char)

        let first = self.src.read_byte().map_err(|err| {
            if matches!(err.kind(), std::io::ErrorKind::UnexpectedEof) {
                DecodeError::CleanEOF
            } else {
                err.into()
            }
        })?;

        if first & 0x80 == 0 {
            Ok(first as char)
        } else if first & 0x40 == 0 {
            Err(DecodeError::UnexpectedContinuationByte)
        } else if first & 0x20 == 0 {
            // 2-byte sequence
            let mut ch = (first as u32 & 0x1F) << 6;
            ch |= self.continuation_byte()?;
            char::from_u32(ch).ok_or(DecodeError::InvalidCharCast)
        } else if first & 0x10 == 0 {
            // 3-byte sequence
            let mut ch = (first as u32 & 0x0F) << 12;
            ch |= self.continuation_byte()? << 6;
            ch |= self.continuation_byte()?;
            char::from_u32(ch).ok_or(DecodeError::InvalidCharCast)
        } else if first & 0x08 == 0 {
            // 4-byte sequence
            let mut ch = (first as u32 & 0x07) << 18;
            ch |= self.continuation_byte()? << 12;
            ch |= self.continuation_byte()? << 6;
            ch |= self.continuation_byte()?;
            char::from_u32(ch).ok_or(DecodeError::InvalidCharCast)
        } else {
            Err(DecodeError::InvalidStartByteFlags)
        }
    }
}
