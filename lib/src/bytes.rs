use std::io::Read;

pub struct ByteReadable<'a> {
    src: &'a mut dyn Read,
}

impl<'a> ByteReadable<'a> {
    pub fn new(src: &'a mut dyn Read) -> Self {
        Self { src }
    }
    pub fn read_byte(&mut self) -> Result<u8, std::io::Error> {
        let mut ch: [u8; 1] = Default::default();
        self.src.read_exact(&mut ch)?;
        Ok(ch[0])
    }
}
