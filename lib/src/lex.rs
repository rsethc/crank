use {
    crate::chars::{CharReadable, DecodeError},
    std::io::Read,
};

pub struct LexState<'a> {
    src: CharReadable<'a>,
    next_ch: Result<char, DecodeError>,
}

#[derive(Debug, Clone, Copy)]
pub enum LexError {
    DecodeError(DecodeError),
    UnrecognizedChar(char),
}

impl From<DecodeError> for LexError {
    fn from(value: DecodeError) -> Self {
        LexError::DecodeError(value)
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Add,
    Sub,
    Mul,
    Div,
    LogicNot,
    Int(i64),
    EOF,
    OpenBrace,
    CloseBrace,
    OpenParen,
    CloseParen,
    Comma,
    Semicolon,
    Identifier(String),
    Let,
    Assign,
    For,
    Func,
}

impl<'a> LexState<'a> {
    pub fn new(src: &'a mut (dyn Read)) -> Self {
        let mut src = CharReadable::new(src);
        Self {
            next_ch: src.read_char(),
            src,
        }
    }
    fn advance(&mut self) {
        self.next_ch = self.src.read_char()
    }

    fn read_int(&mut self) -> Result<Token, LexError> {
        let mut amount = 0;
        loop {
            match self.next_ch.unwrap_or(' ') {
                '0'..='9' => {
                    amount *= 10;
                    amount += self.next_ch? as i64 - '0' as i64;
                    self.advance();
                }
                _ => return Ok(Token::Int(amount)),
            }
        }
    }

    fn read_identifier(&mut self) -> Result<Token, LexError> {
        let mut name = String::new();
        while let Ok(ch) = self.next_ch {
            match ch {
                'a'..='z' | 'A'..='Z' | '0'..='9' | '_' => {
                    name.push(ch);
                    self.advance();
                }
                _ => break,
            }
        }

        match name.as_str() {
            "let" => Ok(Token::Let),
            "for" => Ok(Token::For),
            "func" => Ok(Token::Func),
            _ => Ok(Token::Identifier(name)),
        }
    }

    fn skip_line_comment(&mut self) {
        while self.next_ch.unwrap_or('\n') != '\n' {
            self.advance()
        }
    }

    fn skip_block_comment(&mut self) -> Result<(), LexError> {
        let mut prev = ' ';
        loop {
            let here = self.next_ch?;
            self.advance();
            if prev == '*' && here == '/' {
                return Ok(());
            }
            prev = here;
        }
    }

    fn read_operator(&mut self) -> Result<Token, LexError> {
        match self.next_ch? {
            '+' => {
                self.advance();
                Ok(Token::Add)
            }
            '-' => {
                self.advance();
                Ok(Token::Sub)
            }
            '*' => {
                self.advance();
                Ok(Token::Mul)
            }
            '/' => {
                self.advance();
                match self.next_ch? {
                    '/' => {
                        self.skip_line_comment();
                        self.read_token()
                    }
                    '*' => {
                        self.skip_block_comment()?;
                        self.read_token()
                    }
                    _ => Ok(Token::Div),
                }
            }
            '=' => {
                self.advance();
                Ok(Token::Assign)
            }
            '!' => {
                self.advance();
                Ok(Token::LogicNot)
            }
            '{' => {
                self.advance();
                Ok(Token::OpenBrace)
            }
            '}' => {
                self.advance();
                Ok(Token::CloseBrace)
            }
            '(' => {
                self.advance();
                Ok(Token::OpenParen)
            }
            ')' => {
                self.advance();
                Ok(Token::CloseParen)
            }
            ',' => {
                self.advance();
                Ok(Token::Comma)
            }
            ';' => {
                self.advance();
                Ok(Token::Semicolon)
            }
            unknown => Err(LexError::UnrecognizedChar(unknown)),
        }
    }
    fn skip_spaces(&mut self) {
        loop {
            if let Ok(ch) = self.next_ch {
                if ch.is_whitespace() {
                    self.advance();
                    continue;
                }
            }
            break;
        }
    }
    pub fn read_token(&mut self) -> Result<Token, LexError> {
        self.skip_spaces();
        match self.next_ch {
            Ok(begin) => match begin {
                '0'..='9' => self.read_int(),
                'a'..='z' | 'A'..='Z' | '_' => self.read_identifier(),
                _ => self.read_operator(),
            },
            Err(err) => {
                if matches!(err, DecodeError::CleanEOF) {
                    Ok(Token::EOF)
                } else {
                    Err(err.into())
                }
            }
        }
    }
}
